FROM webdevops/php-nginx:8.1

RUN curl -sS https://getcomposer.org/installer | \
        php -- --install-dir=/usr/local/bin --filename=composer

RUN apt-get update
RUN apt-get -y install git libicu-dev libonig-dev libzip-dev \
        unzip locales libpng-dev libonig-dev libxml2-dev

RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*
RUN locale-gen en_US.UTF-8
RUN localedef -f UTF-8 -i en_US en_US.UTF-8
RUN mkdir /var/run/php-fpm

RUN docker-php-ext-install intl pdo_mysql zip bcmath mbstring \
        exif pcntl bcmath gd

ENV WEB_DOCUMENT_ROOT /app/public

WORKDIR /app

