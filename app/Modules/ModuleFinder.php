<?php

namespace App\Modules;

use Illuminate\Support\Collection;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;
use Symfony\Component\Finder\Finder as SymfonyFinder;

class ModuleFinder
{
    public function __construct(
        private readonly string $basePath = __DIR__
    ) {
    }

    public function serviceProviders(): Collection
    {
        return collect($this->findServiceProvidersFiles())
            ->map(fn (\SplFileInfo $provider) => str_replace('.php', '', $provider->getRealPath()))
            ->map(fn ($path) => Str::replaceFirst(base_path(), '', $path))
            ->map(fn ($path) => Str::replaceFirst('/app/', '/App/', $path))
            ->map(fn ($path) => str_replace('/', '\\', $path))
            ->map(fn ($path) => ltrim($path, '\\'))
            ->filter(fn ($fqcn) => (new \ReflectionClass($fqcn))->isSubclassOf(ServiceProvider::class))
            ->values();
    }

    protected function findServiceProvidersFiles(): SymfonyFinder
    {
        return (new SymfonyFinder())
            ->depth(1)
            ->files()
            ->name('*ServiceProvider.php')
            ->in($this->basePath);
    }
}
