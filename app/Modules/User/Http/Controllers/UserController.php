<?php

namespace App\Modules\User\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\User\Http\Requests\LoginRequest;
use App\Modules\User\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{
    public function show()
    {
        return Auth::user();
    }

    public function login(LoginRequest $loginRequest)
    {
        $user = User::whereEmail($loginRequest->email)->firstOrFail();

        if (!Hash::check($loginRequest->password, $user->password)) {
            return abort(Response::HTTP_UNAUTHORIZED);
        }
    }
}
