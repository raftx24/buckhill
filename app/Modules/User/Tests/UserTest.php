<?php

namespace App\Modules\User\Tests;

use App\Modules\User\Models\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    private User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    public function testShouldFindUser()
    {
        $this->actingAs($this->user)
            ->json('GET', '/api/v1/user/2')
            ->assertJsonPath('id', $this->user->id);
    }

    public function testLoginRequestValidation()
    {
        $this->json('POST', '/api/v1/user/login', [
                'email' => '',
                'password' => '',
            ])
            ->assertJsonValidationErrors(['email', 'password']);
    }

    public function testLogin()
    {
        $this->json('POST', '/api/v1/user/login', [
                'email' => $this->user->email,
                'password' => 'password',
            ])->assertOk();
    }

    public function testWhenPasswordIsWrongShouldReturnError()
    {
        $this->json('POST', '/api/v1/user/login', [
                'email' => $this->user->email,
                'password' => 'wrong-password',
            ])->assertUnauthorized();
    }
}
