<?php

namespace App\Modules\User;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        Route::prefix('api/v1/user')
            ->group(__DIR__.'/api.php');
    }

    public function boot()
    {
    }
}
