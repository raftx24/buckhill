<?php


use App\Modules\User\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::middleware('api')
    ->group(function () {
        //TODO why!?
        Route::get('/2', [UserController::class, 'show']);
    });


Route::post('/login', [UserController::class, 'login']);
