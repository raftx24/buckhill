<?php

namespace App\Modules\Tests;

use App\Modules\ModuleFinder;
use App\Modules\Tests\TestModules\FakeModule\ServiceProvider;

class FinderTest extends \Tests\TestCase
{
    public function testShouldFindServiceProvider()
    {
        $result = [];
        foreach ((new ModuleFinder())->serviceProviders() as $serviceProvider) {
            $result[] = $serviceProvider;
        }

        $this->assertEquals([ServiceProvider::class], (new ModuleFinder(__DIR__.'/TestModules'))->serviceProviders()->toArray());
    }
}
