<?php

namespace App\Modules\Tests\TestModules\FakeModule;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ServiceProvider  extends BaseServiceProvider
{
    public function register()
    {
    }

    public function boot()
    {
    }
}
