<?php

namespace App\Modules;

use Illuminate\Support\ServiceProvider as BaseServiceProvider;

class ModuleServiceProvider extends BaseServiceProvider
{
    public function register()
    {
        (new ModuleFinder())
            ->serviceProviders()
            ->each(fn ($provider) => $this->app->register($provider));
    }

    public function boot()
    {
    }
}
